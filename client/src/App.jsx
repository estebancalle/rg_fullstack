import {Route, Routes} from 'react-router-dom';

// Importamos las vistas
import HomePage from './pages/Home';

import Navbar from './components/Navbar';

function App() {  
  return (
    <>
      <div class="container-fluid">
        <Navbar />
        <br />
        <Routes>
          <Route path='/' element={<HomePage />} />
          {/* <Route path='/new' element={<AutosFormPage />} /> */}
          {/* <Route path='/*' element={<NotFoundPage />} /> */}
        </Routes>
      </div>
    </>
      
  );
}


export default App;
