import { useEffect, useState } from 'react';
import {Form, Formik, Field} from 'formik'
import {obtenerRestaurantesRecomendados,obtenerPlatosRecomendados} from '../services/recomendaciones.services'

// Importamos los componentes que cargan los resultados
import CartaRestaurante from '../components/CartaRestaurante';
import CartaPlato from '../components/CartaPlato';


function HomePage (){

    const [restaurantes, setRestaurantes] = useState([])
    const [platos, setPlatos] = useState([])

    useEffect(()=>{
        async function cargarRestaurantes(){
            const response = await obtenerRestaurantesRecomendados();
            setRestaurantes(response.data)
        }

        async function cargarPlatos(){
            const response = await obtenerPlatosRecomendados();
            setPlatos(response.data)
        }
        
        cargarRestaurantes()
        cargarPlatos()
    },[])


    function validarRestaurantes(){


        if (restaurantes.length > 0) {
            return restaurantes.map(restaurante =>(
                <CartaRestaurante restaurante={restaurante} key={restaurante.idRestaurante} />
            ))
        }
    }

    function validarPlatos(){


        if (platos.length > 0) {
            return platos.map(plato =>(
                <CartaPlato plato={plato} key={plato.idPlato} />
            ))
        }
    }

    return (
        <div>
            
            <div class="card">
                <div class="card-header">
                    Recomendaciones Gastronómicas
                </div>
                <div class="card-body mx-auto">
                    <h5 class="card-title">¿Qué te gustaría comer?, te recomendaremos platos y restaurantes</h5>

                    <Formik
                         initialValues={{
                            gustos: ''
                        }}
                        onSubmit={async (values) => {
                            // await new Promise((r) => setTimeout(r, 500));
                            // alert(JSON.stringify(values, null, 2));
                        //    alert(JSON.stringify(values, null, 2));
                            let a = JSON.stringify(values);
                            a = JSON.parse(a);

                            // const response  = await obtenerRestaurantesRecomendados(a.gustos)

                            
                            localStorage.setItem('palabrasClaves',a.gustos)

                            validarRestaurantes()

                            location.reload()
                            
                            // if (response.data.length > 0) {
                            //     // a = cargarRestaurantesRecomendados(response.data);
                            // }else{
                            //     alert("No hay recomendaciones disponibles");
                            // }
                           
                        }}
                    >
                        <Form>
                            <div class="input-group mb-3">
                                <Field class="form-control" name="gustos" id="gustos" placeholder="Ándale, sin pena" /><br />
                                <button class="btn btn-outline-secondary" type="submit" id="button-addon2">Buscar</button>
                            </div>
                        </Form>
                    </Formik>
                    <br />
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <h3>Restaurantes Recomendados</h3>
                        {validarRestaurantes()}
                    </div>
                    <div className="col-md-6">
                        <h3>Platos recomendados</h3>
                        {validarPlatos()}
                    </div>
                </div>
            </div>

        </div>
    )
}

export default HomePage;