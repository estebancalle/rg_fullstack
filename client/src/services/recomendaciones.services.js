import axios from 'axios';

// CREAR AUTO
// export const creatAuto = async (auto) => await axios.post('http://localhost:4000/nuevoAuto',auto);


// CONSULTAR AUTOS
// export const getAutos =   async () => await axios.get('http://localhost:4242/restaurantes');

// ELIMINAR AUTO
export const obtenerRestaurantesRecomendados = async () => await axios.get('http://localhost:4000/restaurantes/'+localStorage.getItem('palabrasClaves'));
export const obtenerPlatosRecomendados = async () => await axios.get('http://localhost:4000/platos/'+localStorage.getItem('palabrasClaves'));
