import {Link} from 'react-router-dom'


function Navbar (){
    return (
        <div>
           <div class="container-fluid">
                <nav class="navbar navbar-expand-lg bg-light">
                    <div class="container-fluid">
                        <a class="navbar-brand" href="#">Recomendaciones Gastronómicas</a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <Link to="/" class="nav-link active">Inicio</Link>
                            </li>
                            {/* <li class="nav-item">
                                <Link to="/restaurantes" class="nav-link active">Restaurantes</Link>
                            </li>
                            <li class="nav-item">
                                <Link to="/platos" class="nav-link active">Platos</Link>  
                            </li> */}
                        </ul>
                        </div>
                    </div>
                </nav>
           </div>
        </div>
    )
}

export default Navbar;