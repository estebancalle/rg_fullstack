
function CartaRestaurante ({restaurante}){

    const url = restaurante.rutaImagen;

    return(
        <div>
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src="https://www.hotelportonmedellin.com/cache/1a/45/1a45a7ed6dfb3790b9a288c9972f4624.jpg" class="img-fluid rounded-start" alt="..."/>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{restaurante.nombre}</h5>
                            <p class="card-text">{restaurante.ubicacion}</p>
                            <p class="card-text"><small class="text-muted">{restaurante.tipoComida}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    
}

export default CartaRestaurante;