
function CartaPlato ({plato}){


    return(
        <div>
            <div class="card mb-3">
                <div class="row g-0">
                    <div class="col-md-4">
                        <img src={plato.rutaImagen} class="img-fluid rounded-start" alt="..."/>
                    </div>
                    <div class="col-md-8">
                        <div class="card-body">
                            <h5 class="card-title">{plato.nombre}</h5>
                            <p class="card-text">{plato.descripcion}</p>
                            <p class="card-text"><small class="text-muted">{plato.palabrasClave}</small></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )

    
}

export default CartaPlato;