
# RG fullstack

Sistema en React y express para la sugerencia de platos y restaurantes basados
en gustos




## Instalación

Para instalar lo primero que debemos de hacer es instalar las dependencias del backend:

```bash
  npm install
```


Luego de eso instalamos las dependencias del Frontend

```bash
  cd client
  npm install
```

Luego en Mysql importamos la BD que está en la raíz del sistema
recomendaciones_gastronomicas.sql

    