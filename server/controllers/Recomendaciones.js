// Importamos la base de datos
import {conexion} from '../db.js'




// OBTENER RESTAURANTES
export const obtenerRestaurantes = async(req, res) =>{
    
    try {
        const [result] = await conexion.query("SELECT * FROM restaurante")
        res.json(result)
    } catch (error) {
        return res.status(500).send({ message: error.message })
    }

}

export const obtenerPlatos = async(req, res) =>{
    
    try {
        const [result] = await conexion.query("SELECT * FROM plato")
        res.json(result)
    } catch (error) {
        return res.status(500).send({ message: error.message })
    }

}

export const recomendarRestaurantes = async (req, res) =>{
    const [result] = await conexion.query("SELECT * FROM restaurante WHERE tipoComida LIKE '%"+[req.params.palabras]+"%'");

    if(result.length === 0){  
        return res.status(200).json({message: "No tenemos recomendaciones para ti",data:[]});
    }

    res.json(result);
    
}


export const recomendarPlatos = async (req, res) =>{
    const [result] = await conexion.query("SELECT * FROM plato WHERE palabrasClave LIKE '%"+[req.params.palabras]+"%'");

    if(result.length === 0){  
        return res.status(200).json({message: "No tenemos recomendaciones para ti"});
    }

    res.json(result);
    
}
