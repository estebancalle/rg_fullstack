import express from "express";
import {PORT} from './config.js';
import recomendacionesRoutes from './routes/recomendaciones.routes.js';
import cors from 'cors'

const app = express();

app.use(cors())
app.use(express.json())
app.use(recomendacionesRoutes);

app.listen(PORT)
console.log("Port listening on "+PORT);