import { Router } from "express";
import {obtenerRestaurantes,obtenerPlatos,recomendarRestaurantes,recomendarPlatos} from '../controllers/Recomendaciones.js';

const router = Router();



// CRUD PROCESSS
// router.get('/restaurantes',obtenerRestaurantes)
router.get('/platos',obtenerPlatos)
router.get('/restaurantes/:palabras',recomendarRestaurantes)
router.get('/platos/:palabras',recomendarPlatos)

export default router;